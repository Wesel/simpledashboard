<?php

namespace App\System;

class View 
{
	/**
	 * Render the view
	 * 
	 * @param string
	 * @param Array[]
	 */
	public function render($viewPath, $data = [])
	{
		$this->view = $viewPath;
		$this->data = $data;
		require('../views/layout.php');
	}
}
