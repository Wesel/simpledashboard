<?php
namespace App\System;

interface IDatabase
{
	public function getSingleResult($query);
	public function getArrayResult($query);
	public function create($query, $data);
}
