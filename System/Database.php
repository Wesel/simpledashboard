<?php

namespace App\System;

use \PDO;

class Database implements IDatabase
{

	/**
	 * @var string
	 */
	private $host = '127.0.0.1';

	/**
	 * @var string
	 */
	private $db = 'simpledashboard';

	/**
	 * @var string
	 */
	private $user = 'guest';

	/**
	 * @var string
	 */
	private $pass = '';

	/**
	 * @var string
	 */
	private $charset = 'utf8mb4'; 

	/**
	 * PDO
	 */
	private $pdo;

	/**
	 * Constructor initialises Database
	 */
	public function __construct()
	{
		$dsn = "mysql:host=$this->host;dbname=$this->db;charset=$this->charset";
		$options = [
			PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES   => false,
		];
		try {
			$this->pdo = new PDO($dsn, $this->user, $this->pass, $options);
		} catch (\PDOException $e) {
			// TODO Show a more pretty error page
			throw new \PDOException($e->getMessage(), (int)$e->getCode());
		}
	}

	/**
	 * Get Single Result
	 * 
	 * @param string $query
	 * @return mixed
	 */
	public function getSingleResult($query)
	{
		$stmt = $this->pdo->query($query);
		
		return $stmt->fetchColumn();
	}

	/**
	 * Get Array Result
	 * 
	 * @param string $query
	 * @param Array $param
	 * @return string[]
	 */
	public function getArrayResult($query, $params = [])
	{
		$stmt = $this->pdo->prepare($query);
		$stmt->execute($params); 

		return $stmt->fetchAll();
	}

	/**
	 * Insert into database with a given query
	 * 
	 * @param string $query
	 * @param Array $data
	 */
	public function create($query, $data)
	{
		$stmt = $this->pdo->prepare($query);
		$stmt->execute($data);
	}
}
