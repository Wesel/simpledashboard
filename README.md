# README #

### The task ###
Make a sales dashboard as a very simple and straightforward PHP application to show SQL/PHP/JS skills. There is no need to implement all features. More time should be spent on code rather on completing all the tasks.
#### Requirements PHP ####
 - PHP application should be based on MVC structure
 - Have at least one abstract class and one interface
 - Use namespaces
 - PSR-4 standard (http://www.php-fig.org/psr/psr-4/)
 - No PHP framework should be used
 - Use Bootstrap as layout framework
 - Use any JS library/framework if needed (e.g. JQuery, AngularJS)

#### Create a database structure ####
 - Order - purchase date, country, device
 - Order items - EAN, quantity, price
 - Customer - first name, last name, email
 - Customer has 1 to many connection with Order
 - Order has 1 to many connection with Order items

#### Create a simple dashboard that shows statistics for ####
 - Total number of orders
 -  Total number of revenue
 - Total number of customers
 - Statistics by default should be based on last month, with an option to change to any time period (to & from).
 - Create 1 month timeframe chart with customers and orders

## Getting Started ###
  1. Clone Project
  2. Change DB credentials(System/Database.php) or make guest with no password.
  3. Run: composer serve
  4. Open broser: 127.0.0.1:8000
  5. Browse around

### Database Setup ###

```sql
CREATE DATABASE simpledashboard;

CREATE TABLE `Customers` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`email` varchar(255) UNIQUE COLLATE utf8mb4_unicode_ci NOT NULL,
	`created_at` timestamp NULL DEFAULT NULL
);

CREATE TABLE `Orders` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`customer_id` int(10) unsigned NOT NULL,
	`country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`device` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`purchase_date` timestamp NULL DEFAULT NULL,
	`created_at` timestamp NULL DEFAULT NULL,
	FOREIGN KEY (customer_id) REFERENCES Customers(id)
);

CREATE TABLE `OrderItems` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`ean` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
	`quantity` int(10) NOT NULL DEFAULT 1,
	`price` decimal(10,2) NOT NULL,
	`created_at` timestamp NULL DEFAULT NULL
);

CREATE TABLE `OrderOrderItems` (
	`order_id` int(10) unsigned NOT NULL,
	`order_item_id` int(10) unsigned NOT NULL,
	FOREIGN KEY (order_id) REFERENCES Orders(id),
	FOREIGN KEY (order_item_id) REFERENCES OrderItems(id)
);
```
