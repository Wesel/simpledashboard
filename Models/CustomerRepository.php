<?php
namespace App\Models;

use App\Models\DataGenerator;
use App\System\IDatabase;

class CustomerRepository extends Repository
{
	/**
	 * Constructor init Database
	 */
	public function __construct(IDatabase $database)
	{
		$this->database = $database;
	}

	/**
	 *  Get the count of all customers
	 * 
	 * 	@return int
	 */
	public function getTotalCount()
	{
		$query = 'SELECT COUNT(*) FROM Customers';

		return $this->database->getSingleResult($query); 
	}

	/**
	 * Create customer
	 * 
	 * @param int $amount
	 */
	public function create($amount = 1)
	{
		$dataGenerator = new DataGenerator();

		for ($i = 0; $i < $amount; $i ++) {
			$query = 'INSERT INTO `Customers` (
				`firstname`,
				`lastname`,
				`email`,
				`created_at`
			) VALUES (?,?,?,?)';

			$data = [
				$dataGenerator->getRandomString(),
				$dataGenerator->getRandomString(),
				$dataGenerator->getRandomEmail(),
				$dataGenerator->getRandomDate()
			];

			$this->database->create($query, $data);
		}
	}

	/**
	 * Get customer count between two dates
	 * 
	 * @param Date $startDate
	 * @param Date $endDate
	 */
	public function getCustomerCountBetweenDates($startDate, $endDate)
	{
		$query = 'SELECT COUNT(*), CAST(created_at AS DATE)
			FROM Customers 
			WHERE `created_at` BETWEEN ? AND ?  
			GROUP BY CAST(created_at AS DATE)
			ORDER BY CAST(created_at AS DATE)';
		
		return $this->database->getArrayResult($query, [$startDate, $endDate]);
	}
}
