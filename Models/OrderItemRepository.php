<?php
namespace App\Models;

use App\Models\DataGenerator;
use App\System\IDatabase;

class OrderItemRepository extends Repository
{
	/**
	 * Constructor init Database
	 */
	public function __construct(IDatabase $database)
	{
		$this->database = $database;
	}
	/**
	 *  Get the count of order items
	 * 
	 * 	@return int
	 */
	public function getTotalCount()
	{
		$query = 'SELECT COUNT(*) FROM OrderItems';

		return $this->database->getSingleResult($query); 
	}

	/**
	 * Get revenue of all order items
	 * 
	 * @return double
	 */
	public function getTotalRevenue() 
	{ 
		$query = 'SELECT `quantity`, `price` FROM OrderItems';
		$rows = $this->database->getArrayResult($query);
		
		$revenue = 0;
		foreach ($rows as $row) {
			$revenue += $row['quantity'] * $row['price'];
		}

		return $revenue;
	}

	/**
	 * Create order item
	 * 
	 * @param int $amount
	 */
	public function create($amount = 1)
	{ 
		$dataGenerator = new DataGenerator();

		for ($i = 0; $i < $amount; $i ++) { 
			$query = 'INSERT INTO `OrderItems` (
				`ean`,
				`quantity`,
				`price`,
				`created_at`
			) VALUES (?,?,?,?)';

			$data = [
				rand (1000, 10000),
				rand (1, 7),
				rand (10, 1000) / 10,
				$dataGenerator->getRandomDate()
			];

			$this->database->create($query, $data);
		}
	}

	/**
	 * Get order items between two dates
	 * 
	 * @param Date $startDate
	 * @param Date $endDate
	 */
	public function getOrderItemsBetweenDates($startDate, $endDate)
	{
		$query = 'SELECT CAST(created_at AS DATE), quantity, price
			FROM OrderItems
			WHERE `created_at` BETWEEN ? AND ?  
			ORDER BY CAST(created_at AS DATE)';
		
		return $this->database->getArrayResult($query, [$startDate, $endDate]);
	}
}
