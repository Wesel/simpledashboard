<?php
namespace App\Models;

class DataGenerator
{
	/**
	 * Generates a random date within the last year
	 * 
	 * @return Date
	 */ 
	public function getRandomDate()
	{
		$startDate = strtotime(date('Y-m-d H:i:s') . '-6 month');
		$endDate = strtotime(date('Y-m-d H:i:s'));

		$randomTimestamp = mt_rand($startDate, $endDate);

		return date('Y-m-d H:i:s', $randomTimestamp);
	}

	/**
	 * Generates a random string email
	 * 
	 * @return string
	 */
	public function getRandomEmail()
	{
		return $this->getRandomString() . '@gmail.com';
	}

	/**
	 * Generate a random string
	 * 
	 * @param int $lenght
	 * @return string
	 */
	public function getRandomString($length = 8)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}
}
