<?php
namespace App\Models;

abstract class Repository
{   
	abstract public function getTotalCount();
	abstract public function create($amount = 1);
}
