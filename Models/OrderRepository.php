<?php
namespace App\Models;

use App\Models\DataGenerator;
use App\System\IDatabase;

class OrderRepository extends Repository
{
	/**
	 * Constructor init Database
	 */
	public function __construct(IDatabase $database)
	{
		$this->database = $database;
	}

	/**
	 *  Get the count of all customers
	 * 
	 * 	@return int
	 */
	public function getTotalCount(): int
	{
		$query = 'SELECT COUNT(*) FROM Orders';

		return $this->database->getSingleResult($query); 
	}

	/**
	 * Create order
	 * 
	 * @param int $amount
	 */
	public function create($amount = 1)
	{
		$dataGenerator = new DataGenerator(); 
		
		for ($i = 0; $i < $amount; $i ++) { 
			$query = 'INSERT INTO `Orders` (
				`customer_id`,
				`country`,
				`device`,
				`purchase_date`,
				`created_at`
			) VALUES (?,?,?,?,?)';

			$data = [
				rand (1 , 10), // TODO assign real customer id's
				$dataGenerator->getRandomString(2),
				'Mobile',
				$dataGenerator->getRandomDate(),
				$dataGenerator->getRandomDate(),
			];

			$this->database->create($query, $data);
		}
	}

	/**
	 * Get order count between two dates
	 * 
	 * @param Date $startDate
	 * @param Date $endDate
	 */
	public function getOrderCountBetweenDates($startDate, $endDate)
	{
		$query = 'SELECT COUNT(*), CAST(created_at AS DATE)
			FROM Orders 
			WHERE `created_at` BETWEEN ? AND ?  
			GROUP BY CAST(created_at AS DATE)
			ORDER BY CAST(created_at AS DATE)';
		
		return $this->database->getArrayResult($query, [$startDate, $endDate]);
	}
}
