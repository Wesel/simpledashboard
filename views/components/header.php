<nav class="navbar navbar-expand-sm bg-light navbar-light">
	<a class="navbar-brand" href="/">Dashboard</a>
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" href="/customers">Customers</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/orders">Orders</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/orderItems">Order Items</a>
		</li>
	</ul>
</nav>
