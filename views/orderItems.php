<div class="container">
	<div class="row mt-4">
		<div class="col-sm-4 offset-sm-2">
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<h5 class="card-title">Order Items</h5>
					<p class="card-text">
						<?php echo $data['orderItems'] ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 mt-4 pt-1">
			<form action="/orderItems/create" method="POST">
				<button type="submit" class="btn btn-success">Create order item</button>		
			</form>	
		</div>
	</div>
</div>
