<div class="container">
	<div class="row mt-4">
		<div class="col-sm-4 offset-sm-2">
			<div class="card" style="width: 18rem;">
				<div class="card-body">
					<h5 class="card-title">Orders</h5>
					<p class="card-text">
						<?php echo $data['orders'] ?>
					</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 mt-4 pt-1">
			<form action="/orders/create" method="POST">
				<button type="submit" class="btn btn-success">Create order</button>		
			</form>	
		</div>
	</div>

	<p class="mt-4">Orders by day the last 28 days</p>
	<canvas class="my-4 chartjs-render-monitor" id="myChart" width="1392" height="586" style="display: block; height: 293px; width: 696px;"></canvas>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script>
	var ctx = document.getElementById("myChart");
	var myChart = new Chart(ctx, {
		type: 'line',
		data: {
			labels: <?php echo $data['dates'] ?>,
			datasets: [{
				data: <?php echo $data['counts'] ?>,
				lineTension: 0,
				backgroundColor: 'transparent',
				borderColor: '#007bff',
				borderWidth: 4,
				pointBackgroundColor: '#007bff'
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero: false
					}
				}]
			},
			legend: {
				display: false,
			}
		}
	});
</script>
