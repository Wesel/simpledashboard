<?php
 namespace App\Controllers;

 use App\Models\OrderItemRepository;
 use App\System\Database;
 use App\System\View;

 class OrderItemsController extends Controller
 {
 	/**
 	 * Constructor
 	 */
 	public function __construct()
 	{	
 		$this->view = new View();
 		$this->repository = new OrderItemRepository(new Database());
 	}

 	/**
 	 * Show the order items page
 	 */
 	public function index()
 	{
 		$orderItems = $this->repository->getTotalCount();

 		$this->view->render('orderItems', [
 			'orderItems' => $orderItems
 		]);
 	}

 	/**
 	 * Create fake order item
 	 */
 	public function create()
 	{
 		$this->repository->create(3);
 		$this->index();
 	}
 }
