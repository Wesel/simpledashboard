<?php
 namespace App\Controllers;

 use App\Models\OrderRepository;
 use App\System\Database;
 use App\System\View;

 class OrdersController
 {
 	/**
 	 * Constructor
 	 */
 	public function __construct()
 	{	
 		$this->view = new View();
 		$this->repository = new OrderRepository(new Database());
 	}

 	/**
 	 * Show the orders page
 	 */
 	public function index()
 	{
 		$orders = $this->repository->getTotalCount();
 		$graphData = $this->getOrdersByDay();

 		$this->view->render('orders', [
 			'orders' => $orders,
 			'dates' => $graphData[0],
 			'counts' => $graphData[1]
 		]);
 	}

 	/**
 	 * Create fake order
 	 */
 	public function create()
 	{
 		$this->repository->create(3);

 		$this->index();
 	}

 	/**
 	 * Get orders by day for the graph
 	 * 
 	 * @return string[]
 	 */
 	private function getOrdersByDay()
 	{
 		$startDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '-28 days')); 
		$endDate = date('Y-m-d H:i:s');
 		$rows = $this->repository->getOrderCountBetweenDates($startDate, $endDate);

 		$dates = '[';
 		$counts = '[';
 		
 		foreach ($rows as $row) {
 			$dates .= '"' . $row['CAST(created_at AS DATE)'] . '"' . ', ';
 			$counts .= $row['COUNT(*)'] . ', ';
 		}
 		
 		$dates .=  ']';
 		$counts .= ']';

 		return [$dates, $counts]; 
 	}
 }
