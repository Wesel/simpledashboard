<?php
 namespace App\Controllers;

 use App\Models\CustomerRepository;
 use App\Models\OrderRepository;
 use App\Models\OrderItemRepository;
 use App\System\Database;

 class HomeController extends Controller
 {
 	/**
 	 * Show the home page
 	 */
 	public function index()
 	{
 		$customerRepository = new CustomerRepository(new Database());
 		$customers = $customerRepository->getTotalCount();

 		$orderRepository = new OrderRepository(new Database());
 		$orders = $orderRepository->getTotalCount();

 		$orderItemRepository = new OrderItemRepository(new Database());
 		$orderItems = $orderItemRepository->getTotalCount();

 		$totalRevenue = $orderItemRepository->getTotalRevenue();

 		$graphData = $this->getRevenueByDay();

 		$this->view->render('home', [
 			'customers' => $customers,
 			'orders' => $orders,
 			'orderItems' => $orderItems,
 			'totalRevenue' => $totalRevenue,
 			'dates' => $graphData[0],
 			'counts' => $graphData[1]
 		]);
 	}

 	/**
 	 * Get revenue by day for the graph
 	 * 
 	 * @return string[]
 	 */
 	private function getRevenueByDay()
 	{
 		$repository = new OrderItemRepository(new Database());
 		
 		$startDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '-28 days')); 
		$endDate = date('Y-m-d H:i:s');
 		$rows = $repository->getOrderItemsBetweenDates($startDate, $endDate);

 		$dates = '[';
 		$counts = '[';
 		
 		foreach ($rows as $row) {
 			$dates .= '"' . $row['CAST(created_at AS DATE)'] . '"' . ', ';
 			$counts .= $row['quantity']*$row['price'] . ', ';
 		}
 		
 		$dates .=  ']';
 		$counts .= ']';

 		return [$dates, $counts]; 
 	}
 }
