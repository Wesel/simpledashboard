<?php
 namespace App\Controllers;

 class ErrorsController extends Controller
 {
 	/**
 	 * Show Error 404 Page Not Found
 	 */
 	public function error404()
 	{
 		echo "Error 404 Page Not Found";
 	}
 }
