<?php
 namespace App\Controllers;

 use App\Models\CustomerRepository;
 use App\System\Database;
 use App\System\View;

 class CustomersController extends Controller
 {
 	/**
 	 * Constructor
 	 */
 	public function __construct()
 	{	
 		$this->view = new View();
 		$this->repository = new CustomerRepository(new Database());
 	}

 	/**
 	 * Show the customers page
 	 */
 	public function index()
 	{
 		$customers = $this->repository->getTotalCount();
 		$graphData = $this->getCustomersByDay();

 		$this->view->render('customers', [
 			'customers' => $customers,
 			'dates' => $graphData[0],
 			'counts' => $graphData[1]
 		]);
 	}

 	/**
 	 * Create fake customer
 	 */
 	public function create()
 	{
 		$this->repository->create(3);
 		$this->index();
 	}

 	/**
 	 * Get customers by day for the graph
 	 * 
 	 * @return string[]
 	 */
 	private function getCustomersByDay()
 	{
 		$startDate = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '-28 days')); 
		$endDate = date('Y-m-d H:i:s');
 		$rows = $this->repository->getCustomerCountBetweenDates($startDate, $endDate);

 		$dates = '[';
 		$counts = '[';
 		
 		foreach ($rows as $row) {
 			$dates .= '"' . $row['CAST(created_at AS DATE)'] . '"' . ', ';
 			$counts .= $row['COUNT(*)'] . ', ';
 		}
 		
 		$dates .=  ']';
 		$counts .= ']';

 		return [$dates, $counts]; 
 	}
 }
