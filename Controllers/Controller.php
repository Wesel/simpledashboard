<?php

namespace App\Controllers;

use App\System\View;

class Controller 
{
	/**
	 *  Constructor
	 */
	public function __construct()
	{
		$this->view = new View();
	}
}
